package filesystem;

/**
 *  Represents the methods for creating and deleting files,
 *  method for reading is not required
 */
public interface FileSystemAPI {

    /**
     * @param name file name to be created
     * @param data the actual file data
     *
     * @return true, if file successfully saved
     *         false otherwise
     */
    boolean saveFileToHardDrive(String name, byte[] data);


    /**
     * @param name the name of file to delete
     *
     * @return true, if file successfully deleted
     *         false otherwise
     */
    boolean deleteFileFromHardDrive(String name);
}
