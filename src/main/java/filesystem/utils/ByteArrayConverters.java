package filesystem.utils;

/**
 * Supporting class for performing routine operations with byte arrays
 */
public final class ByteArrayConverters {
    public static byte[] convertStringToByteArray(final String name) {
        return name.getBytes();
    }

    public static byte[] convertIntToByteArray(final int value) {
        return new byte[]{
                (byte) ((value >> 24) & 0xFF),
                (byte) ((value >> 16) & 0xFF),
                (byte) ((value >> 8) & 0xFF),
                (byte) (value & 0xFF)};
    }

    public static int convertByteArrayToInt(final byte[] array) {
        return (array[3] & 0xFF) | (array[2] & 0xFF) << 8 | (array[1] & 0xFF) << 16 | (array[0] & 0xFF) << 24;
    }

}
