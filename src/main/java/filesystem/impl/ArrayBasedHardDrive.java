package filesystem.impl;

import filesystem.FileSystemAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static filesystem.utils.ByteArrayConverters.*;
import static java.lang.System.arraycopy;

/**
 * Implementation of the file system running on a hard drive
 * that is represented as a fixed-size byte array
 *
 * No additional data structures or caches are used
 *
 * Name collisions are not supported
 */
public enum ArrayBasedHardDrive implements FileSystemAPI {

    HARD_DRIVE;

    private static final byte FILLED_BIT_MAP_SECTOR = (byte) 255;
    private static final int BIT_MAP_SECTOR_SIZE = 8;
    private static final int ERROR_CODE = -1;
    private static final int OUT_OF_MEMORY = -1;
    private static final int INDEX_NOT_FOUND = -1;
    private static final int ARRAY_START_INDEX = 0;
    private static final int FILE_NAME_MAX_LENGTH = 50;
    private static final int FILE_DATA_MAX_LENGTH = 10;
    private static final int FILE_HASH_CODE_LENGTH = 4;
    private static final int FILE_SYSTEM_RECORD_LENGTH = 60;
    private static final int FILE_SYSTEM_MAX_RECORD_COUNT = 100000;
    private static final int BIT_MAP_SIZE = 12500;
    private static final int HASH_TABLE_SIZE = 400000;
    private static final int DATA_TABLE_START_INDEX = HASH_TABLE_SIZE + BIT_MAP_SIZE;
    private static final int HARD_DRIVE_SIZE = FILE_SYSTEM_MAX_RECORD_COUNT * FILE_SYSTEM_RECORD_LENGTH + BIT_MAP_SIZE + HASH_TABLE_SIZE;

    private static final Logger logger = LoggerFactory.getLogger(ArrayBasedHardDrive.class);

    private final byte[] hardDrive = new byte[HARD_DRIVE_SIZE];

    private int findFreeIndexForRecord() {
        for (int bitMapSectorIndex = 0; bitMapSectorIndex < BIT_MAP_SIZE; bitMapSectorIndex++) {

            final int freePositionInSector = findFreePositionInSector(hardDrive[bitMapSectorIndex]);

            if (freePositionInSector != OUT_OF_MEMORY) {
                return bitMapSectorIndex * BIT_MAP_SECTOR_SIZE + freePositionInSector;
            }
        }

        logger.error("Not enough space left on hard drive, file can not be saved");

        return OUT_OF_MEMORY;
    }

    private int getIndexOfFileHashRecord(final String name) {
        for (int index = BIT_MAP_SIZE; index < HASH_TABLE_SIZE + BIT_MAP_SIZE; index += FILE_HASH_CODE_LENGTH) {

            if (isHashCodePresent(name, readHashCode(index))) {
                return index;
            }
        }

        return ERROR_CODE;
    }

    private boolean isHashCodePresent(final String name, final byte[] hashCode) {
        return convertByteArrayToInt(hashCode) == name.hashCode();
    }

    private byte[] readHashCode(final int startIndex) {
        return new byte[]{hardDrive[startIndex], hardDrive[startIndex + 1], hardDrive[startIndex + 2], hardDrive[startIndex + 3]};
    }

    private synchronized void saveHashCode(final String name, final int startIndex) {
        arraycopy(convertIntToByteArray(name.hashCode()), ARRAY_START_INDEX, hardDrive, startIndex, FILE_HASH_CODE_LENGTH);
    }

    private synchronized void deleteHashCode(final int startIndex) {
        hardDrive[startIndex] = 0;
        hardDrive[startIndex + 1] = 0;
        hardDrive[startIndex + 2] = 0;
        hardDrive[startIndex + 3] = 0;
    }

    private int findFreePositionInSector(final byte bitMapSector) {
        if (bitMapSector == FILLED_BIT_MAP_SECTOR) {
            return ERROR_CODE;
        }
        if ((bitMapSector & 1) == 0) {
            return 0;
        }
        if ((bitMapSector & 2) == 0) {
            return 1;
        }
        if ((bitMapSector & 4) == 0) {
            return 2;
        }
        if ((bitMapSector & 8) == 0) {
            return 3;
        }
        if ((bitMapSector & 16) == 0) {
            return 4;
        }
        if ((bitMapSector & 32) == 0) {
            return 5;
        }
        if ((bitMapSector & 64) == 0) {
            return 6;
        }
        if ((bitMapSector & 128) == 0) {
            return 7;
        }

        return ERROR_CODE;
    }

    private int convertHashCodeIndexToBitMapIndex(final int hashCodeIndex) {
        if (hashCodeIndex - BIT_MAP_SIZE == 0) {
            return 0;
        }

        return 1 << ((hashCodeIndex - BIT_MAP_SIZE) / FILE_HASH_CODE_LENGTH - 1);
    }

    private int convertBitMapIndexToHashCodeIndex(final int bitMapIndex) {
        return BIT_MAP_SIZE + bitMapIndex * FILE_HASH_CODE_LENGTH;
    }

    private void switchStateOfBitMapSectorPosition(final int position) {
        final int sectorNumber = position / BIT_MAP_SECTOR_SIZE;
        final int bitPosition = position % BIT_MAP_SECTOR_SIZE;

        hardDrive[sectorNumber] ^= (1 << bitPosition);
    }

    private byte[] prepareFileToSave(final String name, final byte[] data) {
        final byte[] record = new byte[FILE_SYSTEM_RECORD_LENGTH];
        final byte[] nameArray = convertStringToByteArray(name);

        arraycopy(nameArray, ARRAY_START_INDEX, record, ARRAY_START_INDEX, nameArray.length);

        arraycopy(data, ARRAY_START_INDEX, record, FILE_NAME_MAX_LENGTH, data.length);

        return record;
    }

    private boolean isFileValid(final String name, final byte[] data) {
        if (name.length() > FILE_NAME_MAX_LENGTH) {
            logger.warn("File name too long, file-names up to {} symbols are supported", FILE_NAME_MAX_LENGTH);

            return false;
        }

        if (data.length > FILE_DATA_MAX_LENGTH) {
            logger.warn("File too large to be saved, files up to {} bytes are supported", FILE_DATA_MAX_LENGTH);

            return false;
        }

        return true;
    }

    @Override
    public boolean saveFileToHardDrive(final String name, final byte[] data) {
        if (isFileValid(name, data)) {
            final byte[] newRecord = prepareFileToSave(name, data);

            synchronized (hardDrive) {
                final int indexOfHashRecord = getIndexOfFileHashRecord(name);

                if (indexOfHashRecord == INDEX_NOT_FOUND) {
                    final int freePositionIndex = findFreeIndexForRecord();

                    if (freePositionIndex != ERROR_CODE) {
                        arraycopy(newRecord, ARRAY_START_INDEX, hardDrive,
                                DATA_TABLE_START_INDEX + FILE_SYSTEM_RECORD_LENGTH * freePositionIndex, FILE_SYSTEM_RECORD_LENGTH);
                    }

                    saveHashCode(name, convertBitMapIndexToHashCodeIndex(freePositionIndex));

                    switchStateOfBitMapSectorPosition(freePositionIndex);

                    logger.debug("File \"{}\" successfully created ", name);

                    return true;

                } else {
                    logger.warn("File \"{}\" already exist", name);
                }
            }
        }

        return false;
    }

    @Override
    public boolean deleteFileFromHardDrive(final String name) {
        synchronized (hardDrive) {
            final int indexOfHashCode = getIndexOfFileHashRecord(name);

            if (indexOfHashCode != ERROR_CODE) {
                deleteHashCode(indexOfHashCode);
                switchStateOfBitMapSectorPosition(convertHashCodeIndexToBitMapIndex(indexOfHashCode));

                logger.debug("File \"{}\" successfully deleted", name);

                return true;

            } else {
                logger.warn("File : \"{}\" not found", name);
            }
        }

        return false;
    }

}
